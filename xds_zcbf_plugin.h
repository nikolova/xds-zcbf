/***
  The MIT License (MIT)

  Copyright (c) 2019 Marina Nikolova, EMBL

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
***/


#ifndef XDS_ZCBF_PLUGIN_H
#define XDS_ZCBF_PLUGIN_H


#ifdef __cplusplus
extern "C" {
#endif

    
void plugin_open(const char *file_name,
                 int info[1024],
                 int *error);

void plugin_get_header(int *pixel_count_x, int *pixel_count_y,
                       int *nbytes, float *pixel_size_x, float *pixel_size_y,
                       int *frame_count,
                       int info[1024],
                       int *error);

void plugin_get_data(int *frame_number,
                     int *pixel_count_x, int *pixel_count_y,
                     int *data,
                     int info[1024],
                     int *error);
    
void plugin_close(int *error);

    
#ifdef __cplusplus
} // extern "C" 
#endif


#endif // XDS_ZCBF_PLUGIN_H
