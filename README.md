# xds-zcbf

**XDS plugin for fast reading of cbf.gz files.**


The plugin eliminates the need for multiple file system accesses by way of direct calls to zlib.

Please, note that XDS itself does read cbf.gz files also without this plugin.

To run a March, 2019 or a later XDS version is required.

To use a LIB=/path-to-lib/xds-zcbf.so line is needed in XDS.INP.





# **Prerequisites:**


cmake 3.5 or higher

g++ 5.4.0 or higher



**zlib**  (https://www.zlib.net/)

        Ubuntu:
        
                apt-get install zlib1g-dev
                
        CentOS:
        
                yum install zlib-devel
                
                

**cbflib**  (http://zoomadmin.com/HowToInstall/UbuntuPackage/libcbf-dev)

        Ubuntu:
        
                apt-get install libcbf-dev
                
        CentOS:
        
                yum install CBFlib-devel
                
**zstd** (https://facebook.github.io/zstd)

        Ubuntu:

                apt-get install libzstd-dev

        CentOS:
                yum install libzstd-devel



# **To build:**


mkdir build

cd build

cmake ..

make


The xds-zcbf plugin can be found in the build directory.

To force debug printouts during run, in cmake step above use instead:


cmake -DCMAKE_BUILD_TYPE=Debug ..




# **Platforms:**


Ubuntu


CentOS



# **Possible problems:**


CBFlib might need libhdf5 depending on platform/version. To make sure no dependencies are missing run:


ldd xds-zcbf.so


No 'not found' should be there. An update to the shared library cache (ldconfig) or to LD_LIBRARY_PATH 
might be needed if something shown as dependency is installed but nevertheless 'not found'.



# **Acknowledgements:**


I wish to acknowledge google and its address sanitizer for infallibly saving from armagedon the poor 
souls lacking the propensity to write bug-free code!

Gleb Bourenkov is credited with extensively testing the software, crashing it, and providing indispensable 
insights along every step of the way. The idea to speed up XDS reading of gzipped files is also his.

The XDS 'plugin' method of reading 'random' file formats was started by DECTRIS and the XDS authors. 
The latter were incredibly helpful in updating XDS to make this method usable also for non-hdf5 files.
