/***
  The MIT License (MIT)

  Copyright (c) 2019 Marina Nikolova, EMBL

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
***/


#ifndef XDS_ZCBF_DEFS_H
#define XDS_ZCBF_DEFS_H


#define HUGE_MEM_ALIGNMENT (2 * 1024 * 1024)


#define CUSTOMER_ID         0x00     /* 0x01 -> Dectris; 0x00 -> generic vendor */
#define VERSION_MAJOR       0
#define VERSION_MINOR       1
#define VERSION_PATCH       0
#define VERSION_TIMESTAMP  -1    


#define OUTPUT_INFO         stdout
#define OUTPUT_ERROR        stderr


#define PLUGIN_HEADER      -1
#define PLUGIN_DATA         0

typedef enum {
    COMPRESSION_ZLIB,
    COMPRESSION_ZSTD
} compression_e;


#endif // XDS_ZCBF_DEFS_H
